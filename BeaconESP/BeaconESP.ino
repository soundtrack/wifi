//V3.2

const int espID =2;
#define NB_BEACONS 3
#define BUILTIN_LED 2   //only if using generic boards
#define INT_PIN 0
#define SYNC_EVERY 1 // Time sync every X seconds

const int master = (espID == 0);

int idToSync = 1;

#include "ArduinoJson-v5.11.2.h"

#include <ESP8266WiFi.h>
#include <ArduinoOTA.h>

const char* ssid = "Soundtrack";
const char* password = "123456789";

// Time reference for synchronization. In microseconds
unsigned long syncTime = 0;
unsigned long syncReceived = 0;

//Time variables to avoid delay
unsigned long lastSync = 0;  // to keep sync
unsigned long timeLed = 0;

//Value to send when pulse is detected
unsigned long timePulse = 0;

int clientConnected=0;

DynamicJsonBuffer jsonBuffer;
WiFiUDP udp;
const int syncPort = 12900;
WiFiClient client;
const int port = 12800;
const char* hostServer = "192.168.4.5";

unsigned long realMicros() {
  return (micros() - syncTime);
}

unsigned long computeRealMicros(unsigned long now) {
  return (now - syncTime);
}


void handleInterrupt() {
  timePulse = micros();
}



void masterHandle() {
  //Send a sync packet
  unsigned long elapsed = (realMicros() - lastSync);
  if (elapsed > (SYNC_EVERY * 1000 * 1000)) { // 1000^2 to convert us > s
    idToSync = (idToSync + 1) % NB_BEACONS;
    if (idToSync == 0) {
      idToSync = 1;
    }

    int preSyncbyte = 65535;
    byte hostID = idToSync * 10;
    IPAddress toSendIp(192, 168, 4, hostID);

    // Presync flag
    udp.beginPacket(toSendIp, syncPort);
    udp.print(preSyncbyte);
    udp.endPacket();

    unsigned long wait = micros();
    while ( (unsigned long)(micros() - wait) < (50 * 1000) ) { // wait 50 ms
      yield();
    }

    //Sync packet
    udp.beginPacket(toSendIp, syncPort);
    udp.print(idToSync);
    udp.endPacket();
    unsigned long syncPacket = micros();

    // Dirty repeat, to clean
    wait = micros();
    while ( (unsigned long)(micros() - wait) < (50 * 1000) ) { // wait 50 ms
      yield();
    }

    //Sending value;
    JsonObject& syncJson = jsonBuffer.createObject();
    syncJson["sync"] = computeRealMicros(syncPacket);

    udp.beginPacket(toSendIp, syncPort);
    syncJson.printTo(udp);
    udp.println();
    udp.endPacket();
    yield();

    lastSync = realMicros();
  }

}

void slaveHandle() {
  //check if a 1 byte packet available
  int packetSize = udp.parsePacket();
  if (packetSize == 5) {  // presync received
    udp.flush();
    int waitForSync=1;

    //Serial.println("pres");
    // wait for sync packet
    while ( waitForSync==1 ) { 
      unsigned long capture = micros();
      if (udp.parsePacket() == 1){
        syncReceived = capture;
        waitForSync = 0;
      }

    }

    udp.flush();
    yield();
    
  } else if (packetSize > 0) {
    char packetBuffer[packetSize];
    udp.read(packetBuffer, packetSize);
    JsonObject& root = jsonBuffer.parseObject(packetBuffer);
    if (!root.success()) {
      return;
    }
    if (root.containsKey("sync")) {
      //We received a sync instruction from master
      unsigned long time = root["sync"];
      syncTime = 0;
      syncTime = computeRealMicros(syncReceived) - time;
      syncReceived = 0;
    }
  }
}

void setup() {
  // Serial debug
  Serial.begin(115200);
  Serial.println("Booting");

  //LED
  pinMode(BUILTIN_LED, OUTPUT);

  //PIN interrupt definition
  pinMode(INT_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(INT_PIN), handleInterrupt, FALLING);


  if (master) {
    WiFi.mode(WIFI_AP);
    WiFi.softAP(ssid, password);
  }
  else {
    IPAddress ip(192, 168, 4, (espID * 10));
    IPAddress gateway(192, 168, 4, 1);
    IPAddress subnet(255, 255, 255, 0);
    WiFi.config(ip, gateway, subnet);
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
      pinMode(BUILTIN_LED, HIGH);
      delay(200);
      pinMode(BUILTIN_LED, LOW);
      delay(200);
    }

  }
  pinMode(BUILTIN_LED, HIGH);

  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  udp.begin(syncPort);


}


void loop() {

  if (master) {
    masterHandle();

  } else {
    slaveHandle();
  }

  if(clientConnected==0){
    clientConnected = client.connect(hostServer, port); //return result to remember
  }

  if ( !(timePulse == 0) and (clientConnected==1) ) {
      JsonObject& root = jsonBuffer.createObject();
      root["id"] = espID;
      root["timestamp"] = timePulse;
      root.printTo(client);
      client.println();
      timePulse = 0;
  }

  unsigned long elapsed = (realMicros() - timeLed);
  if (elapsed > (1000) ) { // 1ms refresh loop
    int ledState =  (int)( realMicros() / (1000 * 1000) % 2);
    pinMode(BUILTIN_LED, ledState);
  }

  ArduinoOTA.handle();
}



